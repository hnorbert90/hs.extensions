﻿namespace HS.Extensions.IQueryable
{
    using System;
    using System.Linq;

    using HS.Common.Abstractions.Pagination;

    public static class PaginationExtensions
    {
        public static IQueryable<TEntity> ToPaginatedResult<TEntity>(this IQueryable<TEntity> query, int pageIndex, int pageSize) => query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
        public static IQueryable<TEntity> ToPaginatedResult<TEntity>(this IQueryable<TEntity> query, IPagedQueryRequest request) => query.Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize);
        public static int GetTotalPages<TEntity>(this IQueryable<TEntity> query, IPagedQueryRequest request) => (int)Math.Ceiling((double)query.Count() / request.PageSize);
    }
}